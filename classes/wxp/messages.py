# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Copyright (C) 2017 Federacion Galega de Natación (FEGAN) http://www.fegan.org
# Author: Daniel Muñiz Fontoira (2017) <dani@damufo.com>

import os
import wx
from wx import adv


class Messages:
    """
    Class messages as box dialog
    """

    def __init__(self, view):
        self.view = view

    def entry(self, title, question, default=None):
        """
        for entry a text
        """
        dlg = wx.TextEntryDialog(self.view, question, title)
        if default:
            dlg.SetValue(default)
        if dlg.ShowModal() == wx.ID_OK:
            value = dlg.GetValue()
        else:
            value = False
        dlg.Destroy()
        return value

    def error(self, message=""):
        mns = wx.MessageDialog(self.view, _("Error: {}").format(message),
                               _("error"), wx.ICON_INFORMATION | wx.OK)
        return mns.ShowModal()

    def information(self, message):
        mns = wx.MessageDialog(self.view, message, self.view.GetTitle(),
                               wx.ICON_INFORMATION | wx.OK)
        return mns.ShowModal()

    def success(self):
        msx = wx.MessageDialog(self.view, _("The operation was successful!"),
                               self.view.GetTitle(),
                               wx.ICON_INFORMATION | wx.OK)
        msx.ShowModal()
        msx.Destroy()

    def success_notificantion(self, title=""):
        if not title:
            title = self.view.GetTitle()
        msg = adv.NotificationMessage(title,
                                      _("The operation was successful!"))
        msg.Show()

    def question(self, message, title=None):
        if not title:
            title = self.view.GetTitle()
        mns = wx.MessageDialog(self.view, message, title,
                               wx.ICON_QUESTION | wx.YES_NO)
        if mns.ShowModal() == wx.ID_YES:
            value = True
        else:
            value = False
        mns.Destroy()
        return value

    def warning(self, message, parent_none='False'):
        if parent_none:
            parent = None
        else:
            parent = self.view
        mns = wx.MessageDialog(parent, message, self.view.GetTitle(),
                               wx.ICON_WARNING | wx.OK)
        return mns.ShowModal()

    def choice(self, title, question, values):
        """
        choide dialog
        """
        dlg = wx.SingleChoiceDialog(self.view, question, self.view.GetTitle(),
                                    values, wx.CHOICEDLG_STYLE)
        if dlg.ShowModal() == wx.ID_OK:
            value = (dlg.GetSelection(),  dlg.GetStringSelection())
        else:
            value = None
        dlg.Destroy()
        return value

    def choice_cd(self, title, question, values):
        """
        choide dialog with clientData
        values format: (('value text', key), ('value text', key)...)
        """
        choices = [i[0] for i in values]
        client_data = [i[1] for i in values]

        dlg = wx.SingleChoiceDialog(parent=self.view, message=question,
                                    caption=self.view.GetTitle(),
                                    choices=choices,
                                    style=wx.CHOICEDLG_STYLE,
                                    pos=wx.DefaultPosition)
        if dlg.ShowModal() == wx.ID_OK:
            value = client_data[dlg.GetSelection()]
        else:
            value = None

        dlg.Destroy()
        return value

    def sel_bd_championchip(self, default_path=None):
        sp = wx.StandardPaths.Get()
        x = 'GetDocumentsDir'
        func = getattr(sp, x)
        if not default_path:
            default_path = func()
        self.defaultDir = default_path
        dlg = wx.FileDialog(
            self.view, message="Browse...", defaultDir=self.defaultDir,
            wildcard="Modelos Licenzas (*.mdb)|*.mdb",
            style=wx.FD_OPEN | wx.FD_CHANGE_DIR)
        if dlg.ShowModal() == wx.ID_OK:
            ruta = str(dlg.GetPath())
        else:
            ruta = None
        dlg.Destroy()
        return ruta

    def select_res(self):
        dlg = wx.FileDialog(
            self.view, message="Browse...", defaultDir=".",
            wildcard="File RES (*.res)|*.res",
            style=wx.FD_OPEN | wx.FD_CHANGE_DIR)
        if dlg.ShowModal() == wx.ID_OK:
            ruta = ('%s%s' % (dlg.GetDirectory(), os.sep), dlg.GetFilename())
        else:
            ruta = None
        dlg.Destroy()
        return ruta

    def open_file(self,  message="Browse...", filter_="*", folder_path=None):
        '''
        filter format, tuple(text, extension)
        '''
        if folder_path:
            os.chdir(folder_path)
        dlg = wx.FileDialog(
            self.view, message=message, defaultDir=".",
            wildcard="File %s (*.%s)|*.%s" % (filter_, filter_, filter_),
            style=wx.FD_OPEN | wx.FD_CHANGE_DIR)
        if dlg.ShowModal() == wx.ID_OK:
            ruta_abso = "%s%s%s" % (dlg.GetDirectory(), os.sep,
                                    dlg.GetFilename())
            ruta = (ruta_abso, dlg.GetDirectory(), dlg.GetFilename())
        else:
            ruta = None
        dlg.Destroy()
        return ruta

    def save_file(self, message=None, filter_="*", file_name=None):
        if not message:
            message = _("Save as...")
        dlg = wx.FileDialog(
            self.view, message=message, defaultDir=".",
            wildcard=_("File {} (*.{})|*.{}").format(
                                                    filter_, filter_, filter_),
            style=wx.FD_SAVE | wx.FD_CHANGE_DIR)
        if file_name:
            dlg.SetFilename(file_name)
        if dlg.ShowModal() == wx.ID_OK:
            file_name = dlg.GetFilename()
            file_folder = dlg.GetDirectory()
            if filter != "*":
                if len(file_name) > len(filter_):
                    if file_name[-len(filter_):].lower() != filter_.lower():
                        file_name = "%s.%s" % (file_name, filter_.lower())
                    elif file_name[-len(filter_):] != filter_.lower():
                        file_name = "%s.%s" % (file_name[:-len(filter_)-1],
                                               filter_.lower())
                else:
                    file_name = "%s.%s" % (file_name, filter_.lower())
            ruta_abso = "%s%s%s" % (file_folder, os.sep, file_name)
            ruta = (ruta_abso, file_folder, file_name)
        else:
            ruta = None
        dlg.Destroy()
        return ruta

    def select_folder(self):
        default_folder = os.getcwd()
        dlg = wx.DirDialog(self.view,
                           message="Select a folder",
                           defaultPath=default_folder)
        if dlg.ShowModal() == wx.ID_OK:
            folder = dlg.GetPath()
            os.chdir(dlg.GetPath())
        else:
            folder = None
        return folder
