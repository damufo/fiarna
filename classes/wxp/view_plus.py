# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Copyright (C) 2017 Federacion Galega de Natación (FEGAN) http://www.fegan.org
# Author: Daniel Muñiz Fontoira (2017) <dani@damufo.com>

import wx
from configparser import ConfigParser


class ViewPlus(object):
    '''
    dbs is database connection
    prefs is a prefs file
    '''
    prefs = None
    app_path_folder = None
    file_name = None
    icon = None

    def __init__(self, view):
        '''
        Constructor
        '''
        self.view = view
        self.vars = {}  # View variables

        if not self.prefs:
            self.prefs = ConfigParser()

    def set_var(self, name, value):
        self.vars[name] = value

    def get_var(self, name):
        value = None
        if name in self.vars:
            value = self.vars[name]
        return value

    def save_prefs(self):
        name = self.view.Name
        if not self.prefs.has_section(name):
            self.prefs.add_section(name)

#        position
        pos_x = self.view.GetPosition()[0]
        pos_y = self.view.GetPosition()[1]
        self.prefs.set(name, 'pos_x', str(pos_x))
        self.prefs.set(name, 'pos_y', str(pos_y))
#        size
        width = self.view.Size.width
        height = self.view.Size.height
        self.prefs.set(name, 'width', str(width))
        self.prefs.set(name, 'height', str(height))

        for i in self.vars.keys():
            if i not in ('pos_x', 'pos_y', 'width', 'height'):
                self.prefs.set(name, i, self.vars[i])

        fil = open('%s%s' % (self.app_path_folder, self.file_name), 'w')
        self.prefs.write(fil)
        fil.close()

    def load_prefs(self):
        name = self.view.Name
        if self.prefs.has_section(name):
            for i in self.prefs.options(name):
                self.vars[i] = self.prefs.get(name, i)

#        position
        if self.prefs.has_option(name, 'pos_x'):
            pos_x = self.prefs.getint(name, 'pos_x')
            if pos_x < 0:
                pos_x = 50
            pos_y = self.prefs.getint(name, 'pos_y')
            if pos_y < 0:
                pos_y = 50
#            size = self.get_display_size()
#            if self.view.IsShownOnScreen():
            print('improvement for to work with dual monitors')
            self.view.SetPosition(wx.Point(pos_x, pos_y))
        else:
            self.view.CenterOnScreen()
#        size
        if self.prefs.has_option(name, 'width'):
            width = self.prefs.getint(name, 'width')
            height = self.prefs.getint(name, 'height')
            self.view.SetSize(wx.Size(width, height))
#        icon
        from specific_classes.fegan_ico import getIcon
        self.app_icon = getIcon()
        self.view.SetIcon(self.app_icon)
        # if self.icon:
        #     icon = self.icon
        # elif self.prefs.has_option('general', 'app_icon'):
        #     icon_path = '%s%s' % (self.app_path_folder,
        #                           self.prefs.get('general', 'app_icon'))
        #     icon = wx.Icon(icon_path, wx.BITMAP_TYPE_ICO)
        # if icon:
        #     self.view.SetIcon(icon)

    def get_display_size(self):
            """
            display = wx.GetDisplaySize()
            print display
            rect = wx.ClientDisplayRect()
            print rect
            print wx.Display.GetCount()
            d = wx.Display(0)
            print d.GetGeometry()
            print d.GetClientArea()
            """
            x = 0
            y = 0
            for i in range(wx.Display.GetCount()):
                d = wx.Display(i)
                x += d.ClientArea.Size[0]
                y += d.ClientArea.Size[1]
            return (x, y)

    def cho_get(self, choice):
        '''
        Get choice client data
        '''
        value = None
        if len(choice.Items):
            if choice.GetSelection() is not -1:  # add for wxpython < 2.8
                value = choice.GetClientData(choice.GetSelection())  # wx 2.8
        return value

    def cho_set(self, choice, value):
        '''
        set values when same client data
        '''
        for i in range(choice.GetCount()):
            if str(choice.GetClientData(i)) == str(value):
                choice.Select(i)
                break

    def cho_load(self, choice, values, default=None):
        '''
        set a choice with client data value
        '''
        choice.Clear()
        for i in values:
            if choice.ClassName == 'wxCheckListBox':
                choice.Append(item=i[0])
            else:
                choice.Append(item=i[0], clientData=i[1])
        if default is not None:
            self.cho_set(choice, default)

    def start(self, modal=False):
        self.load_prefs()
        if modal:
            self.view.ShowModal()
        else:
            self.view.Show()

    def stop(self):
        if self.file_name:
            self.save_prefs()
        self.view.Destroy()
