# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Copyright (C) 2017 Federacion Galega de Natación (FEGAN) http://www.fegan.org
# Author: Daniel Muñiz Fontoira (2017) <dani@damufo.com>



import wx
print("wx", wx.VERSION)
import os
import sys
sys.dont_write_bytecode = True

import gettext

from classes.wxp.view_plus import ViewPlus
from specific_classes import fegan_ico
from specific_classes.config import Config
from modules.main.presenter import Presenter

print("wx", wx.VERSION)

class SingleApp(wx.App):
    def OnInit(self):
        # app_path_folder = '{}{}'.format(os.getcwd(), os.sep)
        # ViewPlus.icon = fegan_ico.getIcon()
        if os.name == 'posix':
        #     # print('posix')
        #     app_path_folder = '%s%s' % (os.getcwd(), os.sep)
            platform = "lin"
        else:  # Windows
        #     # print('win')
        #     import subprocess
        #     command = 'cmd.exe /C for %I in ("{}") do echo %~sI'.format(
        #             app_path_folder)
        #     std_out = subprocess.check_output(command)
        #     # print(std_out)
        #     std_out = std_out.decode('ascii').strip().split("\r\n")
        #     app_path_folder = std_out[len(std_out)-1]
            platform = "win"
        # print("ruta", app_path_folder, "ruta")
        app_path_folder = os.path.dirname(os.path.realpath(__file__))
        os.chdir(app_path_folder)
#         print(app_path_folder)
        # check exists prefs if not exists copy prefs_limpo
        # prefs_path = os.path.join(app_path_folder, "prefs.ini") 
        config = Config(app_path_folder=app_path_folder, platform=platform)
        self.localization(language="gl")

        config.app_name = "Fiarna"
        config.app_version = "v. 0.0.1 - 20170309"
        config.app_description = _(
                "Generator of referee's tokens for swimming.")
        config.app_copyright = "(C) 2017 FEGAN (http://www.fegan.org)"
        config.app_web_site = "https://bitbucket.org/damufo/fiarna/"
        config.app_developer = "Daniel Muñiz Fontoira"
        config.app_license = _(
        "Copyright (C) 2017  Galician Federation of Swimming (FEGAN)\n\n"
        "This program is free software: you can redistribute it and/or modify "
        "it under the terms of the GNU General Public License as published by "
        "the Free Software Foundation, either version 3 of the License, or "
        "(at your option) any later version.\n\n"
        "This program is distributed in the hope that it will be useful, "
        "but WITHOUT ANY WARRANTY; without even the implied warranty of "
        "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
        "GNU General Public License for more details.\n\n"
        "You should have received a copy of the GNU General Public License "
        "along with this program.  If not, see <http://www.gnu.org/licenses/>."
        )

        self.name = "Fiarna-%s" % wx.GetUserId()
        self.instance = wx.SingleInstanceChecker(self.name)
        if self.instance.IsAnotherRunning():
            wx.MessageBox(_(u"Another instance is running."), _(u"ERROR"))
            return False

        Presenter(config=config)
#         frame = main.view
#         frame.view  #SingleAppFrame(None, "SingleApp")
#         frame.Show()
        return True

    def localization(self, language="en"):
        '''prepare l10n'''

        filename = "locale/messages_%s.mo" % language

        try:
            trans = gettext.GNUTranslations(open(filename, "rb"))
        except IOError:
            trans = gettext.NullTranslations()
        trans.install()

try:
    app = SingleApp(redirect=False)
    app.MainLoop()
except SystemExit:
    sys.exit(0)
