#!/bin/bash

xgettext --language=Python --from-code=UTF-8 --keyword=_ --output=messages.pot `find /home/damufo/dev/fiarna/ -name "*.py" -not -path "/home/damufo/dev/fiarna/.venv/*"`

exit 0


