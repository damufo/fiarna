��    :      �  O   �      �     �     �                (     5  	   B     L     U     ^  	   g     q  �  w          ,  	   2     <     B     G     V     j     p     �     �  *   �  +   �     	     	     	     	  
   #	     .	     B	  
   S	     ^	  
   e	     p	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     
     
     
     &
  	   5
      ?
  (  `
     �     �      �     �     �     �  	   �     �     �     �  	   �     �  �  �     �     �          
               '     ?     E     a     n  ,   �  -   �  	   �     �     �     �  
             (     ;     P     X     g     �     �  #   �     �  	   �     �     �     �     �     �                              #     ,     4     C  $   L     :             (      $   &           4           "             1           !      ,      )      	   
                       2                    5          /   *   6         8   #   %                           0   9                     .                7   +          -   '       3    About All Another instance is running. Arrival BACKSTSTROKE BREASTSTROKE BUTTERFLY CHRONO 1 CHRONO 2 CHRONO 3 Clasific. Close Copyright (C) 2017  Galician Federation of Swimming (FEGAN)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>. END TIME DEFINITIVE ERROR Error: {} FINAL FREE Fiarna website File {} (*.{})|*.{} Final First close window %s. From event: Generate referee's tokens  Generator of referee's tokens for swimming Generator of referee's tokens for swimming. LANE: {} PRELIM Phase: Points Preliminar RECORDING END TIMES Referee's tokens SPLITS LOG STYLES Save as... Signature timekeeper Sinature secretary desk Sort: The operation was successful! To event: UNIT: {} april august december error february january july june march may november october referee_tokens september {venue} on {month} {day}, {year} Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-11 16:19+0100
Last-Translator: Daniel Muñiz Fontoira <dani@damufo.com>
Language-Team: Galician
Language: gl_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
 Sobre Todas Xa está aberta outra instancia. Chegada COSTAS BRAZA BOLBORETA CRONO 1 CRONO 2 CRONO 3 Clasific. Pechar Copyright © 2017 Federación Galega de Natación  (FEGAN)

Este programa é software libre: vostede pode redistribuílo e/ou modificalo conforme os termos da licenza pública xeral de GNU publicada pola Fundación para o Software Libre, xa sexa a versión 3 desta licenza ou (á súa elección) calquera versión posterior.

Este programa distribúese co desexo de que lle resulte útil, pero SEN GARANTÍAS DE NINGÚN TIPO; nin sequera coas garantías implícitas de VALOR COMERCIAL OU APTITUDE PARA UN PROPÓSITO DETERMINADO. Para máis información, consulte a licenza pública xeral de GNU.

Xunto con este programa, debérase incluír unha copia da licenza pública xeral de GNU. De non ser así, vexa en <http://www.gnu.org/licenses/>. TEMPO FINAL DEFINITIVO ERRO Erro: {} FINAL LIBRE Sitio web Fiarna Ficheiro {} (*.{})|*.{} Final Primeiro pecha a xanela %s. Desde proba: Xerar as fichas de árbitro Xerador de fichas de árbitro para natación Xerador de fichas de árbitro para natación. PISTA: {} PRELIM Fase: Puntos Preliminar REXISTRO DE TEMPOS FINAIS Fichas de árbitro REXISTRO DE PARCIAIS ESTILOS Gardar como... Sinatura do cronometrador Sinatura do secretario de mesa Orde: A operación realizouse con éxito! Até proba: SERIE: {} abril agosto decembro erro febreiro xaneiro xullo xuño marzo maio novembro outubro fichas_arbitro setembro {venue} a {day} de {month} de {year} 