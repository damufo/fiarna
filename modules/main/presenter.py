# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Copyright (C) 2017 Federacion Galega de Natación (FEGAN) http://www.fegan.org
# Author: Daniel Muñiz Fontoira (2017) <dani@damufo.com>

import os

from modules.main.model import Model
from modules.main.view import View
from modules.main.interactor import Interactor


class Presenter(object):

    def __init__(self, config):
        self.model = Model(config)
        self.view = View(self)
        interactor = Interactor()
        interactor.install(self, self.view)
        self.view.view_plus.start()

    def set_path(self):
        os.chdir(self.model.referee_tokens.config.app_path_folder)
        print(os.getcwd())

    def about(self):
        self.view.about(self.model.referee_tokens.config)

    def generate(self):
        from_event, to_event, phase, sort_order = self.view.get_values()
        self.set_path()
        app_path_folder = self.model.referee_tokens.config.app_path_folder
        dbs_path = self.view.msg.sel_bd_championchip(
                default_path=app_path_folder)
        if dbs_path:
            report_path = self.view.msg.save_file(
                    filter_="pdf", file_name=_("referee_tokens"))[0]
            self.model.referee_tokens.sort_order = sort_order     
            self.model.referee_tokens.report(dbs_path=dbs_path,
                                             report_path=report_path,
                                             from_event=from_event,
                                             to_event=to_event,
                                             phase=phase)
            self.view.msg.success()

    def close(self):
        self.view.close()
