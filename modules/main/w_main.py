# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

#import gettext
#_ = gettext.gettext

###########################################################################
## Class Main
###########################################################################

class Main ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = _(u"Fiarna"), pos = wx.DefaultPosition, size = wx.Size( 479,406 ), style = wx.CAPTION|wx.BORDER_NONE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		self.panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.panel.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Tahoma" ) )
		self.panel.SetBackgroundColour( wx.Colour( 149, 187, 226 ) )

		bSizer10 = wx.BoxSizer( wx.VERTICAL )

		bSizer11 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer12 = wx.BoxSizer( wx.VERTICAL )

		bSizer7 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText13 = wx.StaticText( self.panel, wx.ID_ANY, _(u"Fiarna"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText13.Wrap( -1 )

		self.m_staticText13.SetFont( wx.Font( 20, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Sans" ) )

		bSizer7.Add( self.m_staticText13, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT|wx.LEFT, 20 )

		self.m_staticText61 = wx.StaticText( self.panel, wx.ID_ANY, _(u"v. 1.1 - 20220111"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText61.Wrap( -1 )

		bSizer7.Add( self.m_staticText61, 0, wx.ALIGN_BOTTOM|wx.BOTTOM|wx.RIGHT|wx.LEFT, 15 )


		bSizer12.Add( bSizer7, 0, wx.ALIGN_CENTER_HORIZONTAL, 5 )

		self.m_staticText3 = wx.StaticText( self.panel, wx.ID_ANY, _(u"Generator of referee's tokens for swimming"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )

		self.m_staticText3.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_ITALIC, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		bSizer12.Add( self.m_staticText3, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 15 )

		bSizer8 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText7 = wx.StaticText( self.panel, wx.ID_ANY, _(u"From event:"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )

		bSizer8.Add( self.m_staticText7, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.spn_from_event = wx.SpinCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 999, 0 )
		bSizer8.Add( self.spn_from_event, 0, wx.ALL, 5 )


		bSizer12.Add( bSizer8, 0, wx.EXPAND, 5 )

		bSizer81 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText71 = wx.StaticText( self.panel, wx.ID_ANY, _(u"To event:"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText71.Wrap( -1 )

		bSizer81.Add( self.m_staticText71, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.spn_to_event = wx.SpinCtrl( self.panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 999, 99 )
		bSizer81.Add( self.spn_to_event, 0, wx.ALL, 5 )


		bSizer12.Add( bSizer81, 0, wx.EXPAND, 5 )

		bSizer811 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText711 = wx.StaticText( self.panel, wx.ID_ANY, _(u"Phase:"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText711.Wrap( -1 )

		bSizer811.Add( self.m_staticText711, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		cho_phaseChoices = [ _(u"All"), _(u"Final"), _(u"Preliminar") ]
		self.cho_phase = wx.Choice( self.panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, cho_phaseChoices, 0 )
		self.cho_phase.SetSelection( 0 )
		bSizer811.Add( self.cho_phase, 0, wx.ALL, 5 )


		bSizer12.Add( bSizer811, 0, wx.EXPAND, 5 )

		bSizer101 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText72 = wx.StaticText( self.panel, wx.ID_ANY, _(u"Sort:"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText72.Wrap( -1 )

		bSizer101.Add( self.m_staticText72, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		cho_sortChoices = [ _(u"Por pista"), _(u"Por proba") ]
		self.cho_sort = wx.Choice( self.panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, cho_sortChoices, 0 )
		self.cho_sort.SetSelection( 0 )
		bSizer101.Add( self.cho_sort, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer12.Add( bSizer101, 0, wx.EXPAND, 5 )


		bSizer12.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_generate = wx.Button( self.panel, wx.ID_ANY, _(u"Generate referee's tokens "), wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer12.Add( self.btn_generate, 0, wx.ALL|wx.EXPAND, 5 )


		bSizer11.Add( bSizer12, 1, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )


		bSizer10.Add( bSizer11, 1, wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer10.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		bSizer13 = wx.BoxSizer( wx.HORIZONTAL )

		self.btn_about = wx.Button( self.panel, wx.ID_ANY, _(u"About"), wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer13.Add( self.btn_about, 0, wx.ALL, 5 )


		bSizer13.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.btn_close = wx.Button( self.panel, wx.ID_ANY, _(u"Close"), wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer13.Add( self.btn_close, 0, wx.ALL, 5 )


		bSizer10.Add( bSizer13, 0, wx.EXPAND, 5 )


		self.panel.SetSizer( bSizer10 )
		self.panel.Layout()
		bSizer10.Fit( self.panel )
		bSizer1.Add( self.panel, 1, wx.EXPAND |wx.ALL, 0 )


		self.SetSizer( bSizer1 )
		self.Layout()

		self.Centre( wx.BOTH )

	def __del__( self ):
		pass


