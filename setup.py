# -*- coding: utf-8 -*-

"""
usage, execute on console:
$ python setup.py build
"""
import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os"], "excludes": ["tkinter"], 
                     "include_files": ['locale/', 'fonts/']}

#buildOptions = dict(include_files = [(absolute_path_to_your_file,'final_filename')]) #single file, absolute path.
#buildOptions = dict(include_files = ['your_folder/']) #folder,relative path. Use tuple like in the single file to set a absolute path.

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"       

target = Executable(
    script="fiarna.py",
    base=base,
    icon="icon.ico"
    )

setup(
    name="Fiarna",
    version="0.0.1",
    description="Fiarna - Fichas de árbitros de natación",
    author="Daniel Muñiz Fontoira - dani@damufo.com",
    options={"build_exe": build_exe_options},
    executables=[target]
    )